# prostate2

#### 介绍
本项目是人工智能学习的试手项目，主要为理解不同线性回归的效果。调用了python的sklearn库内几个线性回归函数，并使用了python的flask框架做了可视化处理。
当在网页下方的表单内输入要预测的信息后，前端会将数据传向后端处理后将预测的y值显示在前端

#### 软件架构
前端采用html、css，后端采用flask框架


#### 安装教程

1.  下载到本地解压
2.  用pycharm等可编译python文件的app打开
3.  运行app.py
4.  点击出现的网址进入使用即可

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
